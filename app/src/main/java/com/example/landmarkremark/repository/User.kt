package com.example.landmarkremark.repository

import com.google.gson.annotations.SerializedName

data class User (
    @SerializedName ("user_id")
    val uId: String,

    @SerializedName("userName")
    val uName: String,

    val uEmail: String

)