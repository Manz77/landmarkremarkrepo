package com.example.landmarkremark.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.landmarkremark.R
import com.example.landmarkremark.repository.User
import com.example.landmarkremark.viewmodel.FirebaseAuthViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.GoogleAuthProvider
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import java.io.Serializable

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"
    private val RC_SIGN_IN = 11
    lateinit var googleSignInClient: GoogleSignInClient
    lateinit var authViewModel: FirebaseAuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initGoogleSignInClient()
        initSignInButton()
        initAuthViewModel()
    }

    //initialise sign in button
    private fun initSignInButton() {
        google_signIn.setOnClickListener {
            signIn()
        }
    }

    //initialise google sign in client
    private fun signIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    //viewModel
    private fun initAuthViewModel() {
        authViewModel = ViewModelProvider(this).get(FirebaseAuthViewModel::class.java)
    }

    //configure google sign In Options
    private fun initGoogleSignInClient() {
        val googleSignInOptions: GoogleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)!!
                Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
                firebaseAuthWithGoogle(account.idToken!!)
            }catch (e: ApiException){
                Log.e(TAG, "Google sign in failed",e)
            }

        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        signInWithCredential(credential)
        
    }

    private fun signInWithCredential(credential: AuthCredential) {
        authViewModel.signInWithGoogle(credential)

        observeFirebaseViewModel()
    }

    private fun observeFirebaseViewModel(){
        authViewModel.authenticatedUserData.observe(this, Observer { user->
            user?.let { intentToHomeActivity(user) }
            Log.e(TAG, "observeFirebaseViewModel: "+user)
        })
    }

    private fun intentToHomeActivity(user: User) {
        val intent = Intent(this, HomeActivity::class.java)
        intent.putExtra("user", Gson().toJson(user))
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        val currentUser = authViewModel.firebaseAuthRepository.auth.currentUser
        if (currentUser != null){
            val user = User(currentUser.uid, currentUser.displayName!!, currentUser.email !!)
            intentToHomeActivity(user)
        }


    }


}