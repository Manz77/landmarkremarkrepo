package com.example.landmarkremark.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.landmarkremark.R
import com.example.landmarkremark.repository.User
import com.example.landmarkremark.viewmodel.FirebaseAuthViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    private val TAG = "HomeActivity"
    lateinit var authenticatedUser: User
    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.map_menu -> {
                    replaceFragment(MapFragment())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.list_menu -> {
                    replaceFragment(LandmarksListFragment())
                    return@OnNavigationItemSelectedListener true
                }
            }
            false

        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        replaceFragment(MapFragment())
        bottom_nav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        //extract data passed from SignIn Activity
        val jsonString = intent.getStringExtra("user")
        authenticatedUser = Gson().fromJson(jsonString, User::class.java)
        Log.e(TAG, "onCreate: " + authenticatedUser)
    }

    private fun replaceFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container, fragment)
        fragmentTransaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.home_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.signout -> {
                signOut()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //sign out User
    private fun signOut() {
        val authViewModel = ViewModelProvider(this).get(FirebaseAuthViewModel::class.java)
        authViewModel.firebaseAuthRepository.auth.signOut()
    }
}