package com.example.landmarkremark.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.landmarkremark.repository.FirebaseAuthRepository
import com.example.landmarkremark.repository.User
import com.google.firebase.auth.AuthCredential

class FirebaseAuthViewModel: ViewModel() {

    val firebaseAuthRepository= FirebaseAuthRepository()
    val authenticatedUserData : LiveData<User>

    init {
        authenticatedUserData = firebaseAuthRepository.authenticatedUserData
    }

    fun signInWithGoogle(credential: AuthCredential){
        firebaseAuthRepository.firebaseSignInWithGoogle(credential)
    }
}