package com.example.landmarkremark.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth

class FirebaseAuthRepository {

    val auth: FirebaseAuth = FirebaseAuth.getInstance()
    private val _authenticatedUserData = MutableLiveData<User>()
    val authenticatedUserData: LiveData<User>
        get() = _authenticatedUserData

    fun firebaseSignInWithGoogle(credential: AuthCredential){
        auth.signInWithCredential(credential).addOnCompleteListener{
            authTask->
            if (authTask.isSuccessful){
                val firebaseUser = auth.currentUser
                if (firebaseUser != null){
                    val uid = firebaseUser.uid
                    val name = firebaseUser.displayName
                    val email = firebaseUser.email
                    val user = User(uid, name!!, email!!)

                    _authenticatedUserData.postValue(user)

                }
            }else{
                Log.e("Repository", "Sign In Error")
            }
        }
    }
}